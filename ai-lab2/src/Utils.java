import solvers.Solver;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

class Utils {

    Utils() {
    }

    void writeToFile(String methodName, Integer Tmax, Integer maxIterations, Double Tmin, Double alpha, Double rez) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter("C:\\Users\\celin\\Desktop\\ai project\\ia-project\\ai-lab2\\src\\output-file", true));
        String finalRez = methodName + "\t|" + Tmax + "\t|" + maxIterations + "\t\t\t\t|" + Tmin + "\t|" + alpha + "\t|" + Math.floor(rez * 100) / 100;
        writer.newLine();
        writer.write(finalRez);
        writer.newLine();
        writer.close();
    }

    void writeBestAndAverageToFile(Double best, Double avg) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter("C:\\Users\\celin\\Desktop\\ai project\\ia-project\\ai-lab2\\src\\output-file", true));
        String bestRez = "Best: " + Math.floor(best * 100) / 100;
        String avgRez = "Avg: " + Math.floor(avg * 100) / 100;
        writer.write(bestRez);
        List<String> list = Arrays.asList("ff", "ff");
        writer.newLine();
        writer.write(avgRez);
        writer.newLine();
        writer.close();
    }
}
