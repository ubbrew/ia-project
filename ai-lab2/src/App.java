import solvers.AEWithHybridisationSolver;
import solvers.Solver;
import solvers.TSPSolverThreadFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.*;
import solvers.AESolver;

public class App {
    private static List<Double> RESULTS = new ArrayList<>();

    public static void runHybridAE() throws IOException {
        RESULTS = new ArrayList<>();
        System.out.println("Started the AE hybrid algorithm...");
        System.out.println("Running...");
        long start = System.currentTimeMillis();
        Integer numberOfPeople = 50;
        Integer maxGenerations = 1400;
        Integer localSearchIterations = 50;
        int numberOfNeighbours = 300;
        Utils utils = new Utils();

        Solver solver = new AEWithHybridisationSolver("C:\\Users\\celin\\Desktop\\ai project\\ia-project\\ai-lab2\\src\\input-file",
                numberOfPeople,  maxGenerations, localSearchIterations, numberOfNeighbours);
        ExecutorService executorService = Executors.newCachedThreadPool(new TSPSolverThreadFactory());
        CompletionService<Double> taskManager = new ExecutorCompletionService<>(executorService);
        for (int i = 0; i < 10; i++) {
            taskManager.submit(solver);
            solver = new AEWithHybridisationSolver("C:\\Users\\celin\\Desktop\\ai project\\ia-project\\ai-lab2\\src\\input-file",
                    numberOfPeople,  maxGenerations, localSearchIterations, numberOfNeighbours);
        }

        executorService.shutdown();
        String solvingMethodName = solver.getClass().getSimpleName().replace("Solver", "");

        for(int i=0; i< 10; i++){
            try {
                Double value =  taskManager.take().get();
                RESULTS.add(value);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        Double best = RESULTS.stream()
                .mapToDouble(val->val)
                .min()
                .orElseThrow(NoSuchElementException::new);
        Double sum = RESULTS.stream()
                .mapToDouble(val->val)
                .sum();
        Double avg = sum / RESULTS.size();
        utils.writeBestAndAverageToFile(best, avg);
        System.out.println(solvingMethodName);
        long finish = System.currentTimeMillis() - start;
        System.out.println("Finished executing in: " + finish + "ms");
    }
    
    public static void runAE() throws IOException {
        RESULTS = new ArrayList<>();
        System.out.println("Started the AE algorithm...");
        System.out.println("Running...");
        long start = System.currentTimeMillis();
        Integer numberOfPeople = 75;
        Integer maxGenerations = 500;
        String crossType = "ox";
        String mutationType="inversion";
        Utils utils = new Utils();
        Solver solver = new AESolver("C:\\Users\\sebi\\eclipse-workspace\\ia-project\\ai-lab2\\src\\pr76.tsp",
                numberOfPeople,  maxGenerations, crossType, mutationType);
        ExecutorService executorService = Executors.newCachedThreadPool(new TSPSolverThreadFactory());
        CompletionService<Double> taskManager = new ExecutorCompletionService<>(executorService);
        for (int i = 0; i < 1; i++) {
            taskManager.submit(solver);
            solver = new AESolver("C:\\Users\\sebi\\eclipse-workspace\\ia-project\\ai-lab2\\src\\pr76.tsp",
                    numberOfPeople,  maxGenerations, crossType, mutationType);
        }

        executorService.shutdown();
        String solvingMethodName = solver.getClass().getSimpleName().replace("Solver", "");

        for(int i=0; i< 1; i++){
            try {
                Double value =  taskManager.take().get();
                RESULTS.add(value);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        Double best = RESULTS.stream()
                .mapToDouble(val->val)
                .min()
                .orElseThrow(NoSuchElementException::new);
        Double sum = RESULTS.stream()
                .mapToDouble(val->val)
                .sum();
        Double avg = sum / RESULTS.size();
        utils.writeBestAndAverageToFile(best, avg);
        System.out.println(solvingMethodName);
        long finish = System.currentTimeMillis() - start;
        System.out.println("Finished executing in: " + finish + "ms");
    }

    public static void main(String[] args) throws IOException {
//    	runAE();
    	runHybridAE();
    	
    }
}
