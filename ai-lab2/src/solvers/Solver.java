package solvers;

import model.Town;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.concurrent.Callable;

public abstract class Solver implements Callable<Double> {
    List<Town> towns;
    Random random = new Random();
    private double[][] distances;

    Solver(String fileName) throws FileNotFoundException {
        towns = new ArrayList<>();
        loadDataFromFile(fileName);
        distances = calculateDistancesBetweenTowns();
    }

    public abstract double solveTSP();

    private double[][] calculateDistancesBetweenTowns() {
        double[][] distances = new double[towns.size()][towns.size()];
        for (int i = 0; i < towns.size(); i++) {
            for (int j = 0; j < towns.size(); j++) {
                distances[i][j] = calculateTheEuclideanDistanceBetweenCities(towns.get(i), towns.get(j));
            }
        }
        return distances;
    }

    private void loadDataFromFile(String fileName) throws FileNotFoundException {
        String[] line;
        Scanner scanner = new Scanner(new File(fileName));
        for (int i = 0; i < 6; i++) //get over the details from file
            scanner.nextLine();
        while (scanner.hasNext()) {
            line = scanner.nextLine().split("\\s+");
            if (line[0].equals("EOF"))
                break;
            int index = Integer.parseInt(line[0]) - 1;
            double x = Double.parseDouble(line[1]);
            double y = Double.parseDouble(line[2]);
            Town town = new Town(index, x, y);
            towns.add(town);
        }
    }

    double getTheFitnessOfASolution(List<Integer> solution) {
        List<Town> townList = new ArrayList<>();
        for (int i = 0; i < solution.size(); i++) {
            int finalI = i;
            townList.add(towns.stream().filter(town -> town.getId() == solution.get(finalI)).findFirst().get());
        }
        double fitness = 0;
        for (int j = 0; j < townList.size() - 1; j++) {
            Town town1 = townList.get(j);
            Town town2 = townList.get(j + 1);
            fitness += distances[town1.getId()][town2.getId()];
        }
        fitness += distances[townList.get(townList.size() - 1).getId()][townList.get(0).getId()];
        return fitness;
    }

    private double calculateTheEuclideanDistanceBetweenCities(Town source, Town destination) {
        double xComponent = destination.getX() - source.getX();
        double yComponent = destination.getY() - source.getY();
        return Math.sqrt(xComponent * xComponent + yComponent * yComponent);
    }

    List<Integer> getRandomNeighbourUsing2swap(List<Integer> sol) {
        List<Integer> neighbour = new ArrayList<>(sol);
        int i, j = 0;
        i = random.nextInt(sol.size());
        boolean found = false;
        while (!found) {
            j = random.nextInt(sol.size());
            if (i != j)
                found = true;
        }
        Collections.swap(neighbour, i, j);
        return neighbour;
    }

    List<Integer> getRandomNeighbourUsing2opt(List<Integer> sol){
        int i, j;
        j = 1 + random.nextInt(sol.size() -1);
        i = random.nextInt(j);
        while (i <= j){
            Collections.swap(sol, i, j);
            i++;
            j--;
        }
        return sol;
    }

    List<Integer> generateRandomSolution() {
        List<Integer> indexes = new ArrayList<>();
        for (int i = 0; i < towns.size(); i++) {
            indexes.add(i);
        }
        Collections.shuffle(indexes);
        return indexes;
    }

    List<Integer> generateGreedySolution() {
        Map<Integer, Boolean> visitedTowns = new HashMap<>();
        List<Integer> greedySolution = new ArrayList<>(Collections.nCopies(towns.size(),0));
        for (Town town : towns) {
            visitedTowns.put(town.getId(), false);
        }
        int currentIteration = 0;
        int currentTownIndex = 0;
        int nextTownIndex = 0;
        greedySolution.set(currentTownIndex, 0);
        visitedTowns.put(currentTownIndex, true);
        while(currentIteration < towns.size()-1){
            double shortestDistance = Double.MAX_VALUE;
            for (int i = 0; i < towns.size(); i++) {
                //if we didn't visit the town
                if(!visitedTowns.get(i) && currentTownIndex!=i){
                    if(distances[currentTownIndex][i] < shortestDistance){
                        shortestDistance = distances[currentTownIndex][i];
                        nextTownIndex = i;
                    }
                }
            }
            visitedTowns.put(nextTownIndex, true);
            currentTownIndex = nextTownIndex;
            currentIteration++;
            greedySolution.set(currentIteration, currentTownIndex);
        }
        return greedySolution;
    }
}
