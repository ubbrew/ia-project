package solvers;

import java.io.FileNotFoundException;
import java.util.*;
import java.util.concurrent.Callable;

public class AEWithHybridisationSolver extends Solver implements Callable<Double> {

    private static final Random RANDOM = new Random();
    private int numberOfPeople, maxGenerations;
    private int localSearchIterations;
    private int numberOfNeighbours;

    public AEWithHybridisationSolver(String fileName, int numberOfPeople, int maxGenerations,
                                     int localSearchIterations, int numberOfNeighbours)
            throws FileNotFoundException {
        super(fileName);
        this.numberOfPeople = numberOfPeople;
        this.maxGenerations = maxGenerations;
        this.localSearchIterations = localSearchIterations;
        this.numberOfNeighbours = numberOfNeighbours;

    }


    public double solveTSP() {
        int currentGeneration = 0;
        double best, currentBest;
        List<List<Integer>> parents, crossPeople, crossPeopleWithSecondMethod, mutationsPeople;
        List<List<Integer>> population = generateRandomPopulation(numberOfPeople);
        best = getTheBestFitnessFromPopulation(population);
        while (currentGeneration < maxGenerations) {
            parents = selectParentsFromCurrentPopulation(population, 2 * numberOfPeople / 3);
            crossPeople = getCrossedPopulation(parents);
            crossPeopleWithSecondMethod = crossover(parents);
            mutationsPeople = mutatePopulation(parents, crossPeople);
            population = selectSurvivorsForTheNextGeneration(numberOfPeople, parents, crossPeople, crossPeopleWithSecondMethod, mutationsPeople, population);
            currentBest = getTheBestFitnessFromPopulation(population);
            best = Math.min(best, currentBest);
            currentGeneration++;
        }
        return best;
    }

    private List<List<Integer>> selectSurvivorsForTheNextGeneration(int numberOfPeople, List<List<Integer>> parents, List<List<Integer>> crossPeople, List<List<Integer>> crossPeopleWithSecondMethod,  List<List<Integer>> mutationsPeople, List<List<Integer>> population) {
        List<List<Integer>> candidates = new ArrayList<>();
        List<List<Integer>> newPopulation = new ArrayList<>();
        candidates.addAll(parents);
        candidates.addAll(crossPeople);
        candidates.addAll(crossPeopleWithSecondMethod);
        candidates.addAll(mutationsPeople);
        candidates.addAll(population);
        Comparator<List<Integer>> comparator = Comparator.comparingDouble(this::getTheFitnessOfASolution);
        candidates.sort(comparator);
        int candidatesIndex = 0;
        int newPeopleIndex = 0;
        while(newPeopleIndex != numberOfPeople) {
            if(!newPopulation.contains(candidates.get(candidatesIndex))) {
                newPopulation.add(candidates.get(newPeopleIndex));
                newPeopleIndex++;
            }
            candidatesIndex++;
        }

        return newPopulation;
    }

    private List<List<Integer>> mutatePopulation(List<List<Integer>> parents, List<List<Integer>> crossPeople) {
        List<List<Integer>> currentPopulation = new ArrayList<>();
        currentPopulation.addAll(parents);
        currentPopulation.addAll(crossPeople);
        List<List<Integer>> mutations = new ArrayList<>();
        for (List<Integer> solution : currentPopulation) {
            mutations.add(getRandomNeighbourUsing2swap(solution));
        }
        return mutations;
    }

    private List<List<Integer>> getCrossedPopulation(List<List<Integer>> parents) {
        List<List<Integer>> crossedPopulation = new ArrayList<>();
        int firstCutPoint, secondCutPoint;

        for (int i = 0; i < parents.size() - 1; i++) {
            List<Integer> firstChromosome = new ArrayList<>(Collections.nCopies(towns.size(), -1)),
                    secondChromosome = new ArrayList<>(Collections.nCopies(towns.size(), -1));
            List<Integer> firstParent = new ArrayList<>(parents.get(i));
            List<Integer> secondParent = new ArrayList<>(parents.get(i + 1));
            //get a cutting point between 1 and length/4
            firstCutPoint = RANDOM.nextInt(parents.get(i).size() / 4) + 1;
            secondCutPoint = RANDOM.nextInt(parents.get(i).size() - 1 - firstCutPoint) + firstCutPoint;
            //between the 2 cut points, inherit from parent
            for (int j = firstCutPoint; j < secondCutPoint; j++) {
                firstChromosome.set(j, firstParent.get(j));
                secondChromosome.set(j, secondParent.get(j));
            }
            int currentIndex = secondCutPoint;
            int parentIndex = secondCutPoint;
            while (currentIndex < secondParent.size()) {
                if (parentIndex == secondParent.size()) {
                    parentIndex = 0;
                }
                if (!firstChromosome.contains(secondParent.get(parentIndex))) {
                    firstChromosome.set(currentIndex, secondParent.get(parentIndex));
                    currentIndex++;
                }
                parentIndex++;
            }
            currentIndex = 0;
            parentIndex = secondCutPoint;
            while (currentIndex < firstCutPoint) {
                if (parentIndex == secondParent.size()) {
                    parentIndex = 0;
                }
                if (!firstChromosome.contains(secondParent.get(parentIndex))) {
                    firstChromosome.set(currentIndex, secondParent.get(parentIndex));
                    currentIndex++;
                }
                parentIndex++;
            }
            currentIndex = secondCutPoint;
            parentIndex = secondCutPoint;

            while (currentIndex < firstParent.size()) {
                if (parentIndex == firstParent.size()) {
                    parentIndex = 0;
                }
                if (!secondChromosome.contains(firstParent.get(parentIndex))) {
                    secondChromosome.set(currentIndex, firstParent.get(parentIndex));
                    currentIndex++;
                }
                parentIndex++;
            }
            currentIndex = 0;
            parentIndex = secondCutPoint;
            while (currentIndex < firstCutPoint) {
                if (parentIndex == firstParent.size()) {
                    parentIndex = 0;
                }
                if (!secondChromosome.contains(secondParent.get(parentIndex))) {
                    secondChromosome.set(currentIndex, secondParent.get(parentIndex));
                    currentIndex++;
                }
                parentIndex++;
            }
            crossedPopulation.add(firstChromosome);
            crossedPopulation.add(secondChromosome);
        }
        return crossedPopulation;
    }

    /*
   Descr: Applies cycle crossover on a list of parents
   Parans: a list of parents
    */
    private List<List<Integer>> crossover(List<List<Integer>> parents) {
        List<List<Integer>> children = new ArrayList<>();
        List<Integer> firstParent, secondParent, firstChild, secondChild;
        int i, j, searchedPos, currentPos;
        boolean cycleDone;
        for(i = 0; i<parents.size()-1; i++) { //iterate the list of parents
            firstParent = parents.get(i);
            secondParent = parents.get(i+1);
            firstChild = new ArrayList<>(Collections.nCopies(firstParent.size(), -1)); //initialize two descendants with zeros
            secondChild = new ArrayList<>(Collections.nCopies(secondParent.size(), -1));
            //creating the first child here
            cycleDone = false;
            j = 0;
            while (!cycleDone) {
                firstChild.set(j, firstParent.get(j));
                searchedPos = secondParent.get(j); //the position we need to search for as a value in firstParent list
                currentPos = 0;
                while(searchedPos != firstParent.get(currentPos)) {
                    currentPos++;
                }
                cycleDone = secondParent.get(j).equals(firstParent.get(0)); //check if the cycle has been completed
                j = currentPos;
            }
            for(j = 0; j<firstChild.size(); j++) { //the cycle has been completed, but there are still zeros to be replaced
                if(firstChild.get(j) == -1) { //each zero is being replaced with a value from the secondParent list
                    firstChild.set(j, secondParent.get(j));
                }
            }
            //creating the second child similarly
            cycleDone = false;
            j = 0;
            while (!cycleDone) {
                secondChild.set(j, secondParent.get(j));
                searchedPos = firstParent.get(j);
                currentPos = 0;
                while(searchedPos != secondParent.get(currentPos)) {
                    currentPos++;
                }
                cycleDone = firstParent.get(j).equals(secondParent.get(0));
                j = currentPos;
            }
            for(j=0; j<secondChild.size(); j++) {
                if(secondChild.get(j) == -1) {
                    secondChild.set(j, firstParent.get(j));
                }
            }
            children.add(firstChild);
            children.add(secondChild);
        }
        return children;
    }

    private List<List<Integer>> selectParentsFromCurrentPopulation(List<List<Integer>> population, int numberOfParents) {
        List<List<Integer>> parents = new ArrayList<>();
        List<List<Integer>> contestants;
        int parentsSize = 0;
        List<Integer> nextParent;
        while (parentsSize < numberOfParents) {
            contestants = new ArrayList<>();
            for (int j = 0; j < 2; j++) {
                contestants.add(population.get(RANDOM.nextInt(population.size())));
            }
            nextParent = getTheParentFromContestants(contestants);
            parents.add(nextParent);
            parentsSize++;
        }
        return parents;
    }

    private List<Integer> getTheParentFromContestants(List<List<Integer>> contestants) {
        int nextParentIndex = 0;
        double currentFitness;
        double maxFitness = getTheFitnessOfASolution(contestants.get(0));
        for (int currentIndex = 1; currentIndex < contestants.size(); currentIndex++) {
            currentFitness = getTheFitnessOfASolution(contestants.get(currentIndex));
            if (maxFitness < currentFitness) {
                maxFitness = currentFitness;
                nextParentIndex = currentIndex;
            }
        }
        return contestants.get(nextParentIndex);
    }

    private double getTheBestFitnessFromPopulation(List<List<Integer>> population) {
        return population
                .stream()
                .map(this::getTheFitnessOfASolution)
                .mapToDouble(fitness -> fitness)
                .min()
                .getAsDouble();
    }

    private List<List<Integer>> generateRandomPopulation(int numberOfPeople) {
        List<List<Integer>> randomPopulation = new ArrayList<>();
        for (int i = 0; i < numberOfPeople ; i++) {
            List<Integer> randomSolution = hybridWithLocalSearch();
            randomPopulation.add(randomSolution);
        }
        return randomPopulation;
    }

    private List<Integer> hybridWithLocalSearch() {
        List<Integer> solution = generateRandomSolution();
        List<Integer> best = new ArrayList<>(solution);
        List<Integer> currentNeighbour;
        for(int i = 0; i < localSearchIterations; i++) {
            currentNeighbour = getTheBestNeighbourOfSolution(solution);
            if(Double.compare(getTheFitnessOfASolution(solution), getTheFitnessOfASolution(currentNeighbour)) > 0) {
                solution = new ArrayList<>(currentNeighbour);
                if(getTheFitnessOfASolution(solution) < getTheFitnessOfASolution(best)){
                    best = new ArrayList<>(solution);
                }
            }
            else {
                solution = generateRandomSolution();
            }
        }
        return best;
    }

    private  List<Integer> getTheBestNeighbourOfSolution(List<Integer> solution) {
        List<Integer> currentNeighbour ;
        List<Integer> best = new ArrayList<>(solution);
        for(int i = 0; i < numberOfNeighbours;i++) {
            currentNeighbour = new ArrayList<>(solution);
            int k = RANDOM.nextInt(solution.size() - 1);
            int j = 0;
            boolean found = false;
            while (!found) {
                j = random.nextInt(solution.size());
                if (k != j)
                    found = true;
            }
            Collections.swap(solution, j , k);
            if(getTheFitnessOfASolution(currentNeighbour) < getTheFitnessOfASolution(solution)) {
                best = new ArrayList<>(currentNeighbour);
            }

        }
        return best;
    }


    @Override
    public Double call() {
        System.out.println("Started Thread: " + Thread.currentThread().getName());
        Double result = solveTSP();
        System.out.println(Thread.currentThread().getName() + " finished working... RESULT: " + result);
        return result;
    }
}
