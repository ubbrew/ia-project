package solvers;

import java.util.Random;
import java.util.concurrent.Callable;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
 


public class AESolver extends Solver implements Callable<Double>{
	private static final Random RANDOM = new Random();
    private int N,M;
    private String crossType, mutationType;
    
    public AESolver(String fileName, int N, int M, String crossType, String mutationType) 
    		throws FileNotFoundException {
    		super(fileName);
		 this.N=N;
		 this.M=M;
		 this.crossType=crossType;
		 this.mutationType=mutationType;
		 }
    
	    /*
		 * TSP EA Solver
		 */
		public double solveTSP(){
			int t=0;
			List<List<Integer>> parents,px,pm;
			List<List<Integer>> p = populate(N);
			double best = getBestIndividual(p);
			double c;
			while(t<M) {
				parents=selectParents(p);
				px=cross(parents,crossType);
				pm=mutation(px,mutationType);
				p=survivalSelection(parents,px,pm,N);
				c = getBestIndividual(p);
				best = Math.min(best, c);
				t++;
			}
	        return best;
		 }
		
		/*
		 * Simulated annealing algorithm
		 */
		List<Integer> sa(double T, double alpha, double minT, int nrit){
			List<Integer> c = generateRandomSolution();
			List<Integer> best = new ArrayList<>(c);
			while(T>minT) {
				for(int i=0;i<nrit;i++) {
					List<Integer> x = getRandomNeighbourUsing2swap(c);
					double delta = getTheFitnessOfASolution(x) - getTheFitnessOfASolution(c);
					if(delta<0) {
						c = new ArrayList<>(x);
						if(getTheFitnessOfASolution(best)>getTheFitnessOfASolution(c))
							best = new ArrayList<>(c);
						else if(Math.random()<Math.exp(-delta/T))
							c = new ArrayList<>(x);
					}
					T=alpha*T;
				}
			}
			return best;
		}
		
		/*
		 * mutation algorithm(swap/inversion/scramble)
		 *IN: population, mutation type
		 *OUT: population resulted by mutation
        */
		List<List<Integer>> mutation(List<List<Integer>>p, String type){
			int n=p.size();
			int size=p.get(0).size();
			List<List<Integer>> l = new ArrayList<>();
			for(int i=0;i<n;i++) {
				int q1 = RANDOM.nextInt(size-2)+1;
				int q2 = RANDOM.nextInt(size - 1 - q1) + q1;
				List<Integer> x = new ArrayList<>(p.get(i));
				//scramble method
				if(type.equals("scramble")) {
					List<Integer> aux = new ArrayList<>();
					for(int j=q1;j<=q2;j++)
						aux.add(x.get(j));
					shuffle(aux);
					for(int j=q1;j<=q2;j++) {
						x.set(j, aux.get(0));
						aux.remove(0);
					}
				}
				//swap method
					else if(type.equals("swap")) {
						int aux = x.get(q1);
						x.set(q1,x.get(q2));
						x.set(q2, aux);
					}
				//inversion method
					else if(type.equals("inversion")) {
						List<Integer> aux = new ArrayList<>();
						for(int j=q1;j<=q2;j++)
							aux.add(x.get(j));
						Collections.reverse(aux);
						for(int j=q1;j<=q2;j++) {
							x.set(j, aux.get(0));
							aux.remove(0);
						}
					}
				l.add(x);
				}
			return l;	
			}
		
		/*
		 * Populate method
		 * IN: nr of individuals
		 * OUT: population resulted by generated individuals
		 */
		private List<List<Integer>> populate(int N) {
	        List<List<Integer>> l = new ArrayList<>();
	        //select 10% by sa algorithm
	        int n = (int)(N*0.1);
	        for (int i = 0; i < n; i++)
	        	l.add(sa(1000,0.999,0.1,10));
	        //select another 90% random
	        for (int i = n; i < N; i++) {
	        	List<Integer> solution = generateRandomSolution();
	        	shuffle(solution);
	            l.add(solution);
	        }
	        return l;
	    }
		
		//a simple shuffle algorithm for List
		static void shuffle(List<Integer> ar)
		  {
		    Random rnd = ThreadLocalRandom.current();
		    for (int i = ar.size() - 1; i > 0; i--)
		    {
		      int index = rnd.nextInt(i + 1);
		      // Simple swap
		      int a = ar.get(index);
		      ar.set(index,  ar.get(i)) ;
		      ar.set(i, a);
		    }
		  }
		
		/*
		 * Cross 
		 * IN: initial population, cross type
		 * OUT: population resulted by chosen cross method
		 */
		List<List<Integer>> cross(List<List<Integer>> parents, String crossType){
			List<List<Integer>> childrens = new ArrayList<>();
			if(crossType.equals("ox")) {
				for (int i = 0; i < parents.size() - 1; i+=2) {
					List<List<Integer>> res = oxCross(parents.get(i),parents.get(i+1));
					childrens.add(res.get(0));
					childrens.add(res.get(1));
				}
			}
			else if(crossType.equals("pmx")) {
				for (int i = 0; i < parents.size() - 1; i++) {
					List<List<Integer>> res = pmxCross(parents.get(i),parents.get(i+1));
					childrens.add(res.get(0));
					childrens.add(res.get(1));
				}
				
			}
			return childrens;
			}
		
		/*
		 * Order Crossover
		 * IN: two parents
		 * OUT: two chromosomes resulted by order cross
		 */
		List<List<Integer>> oxCross(List<Integer> p1,List<Integer> p2){
			List<Integer> c1 = new ArrayList<>(Collections.nCopies(towns.size(), -1)),
                    c2 = new ArrayList<>(Collections.nCopies(towns.size(), -1));
			List<Integer> l1 = new ArrayList<>(),l2= new ArrayList<>();
			int size=p1.size();
			int q1 = RANDOM.nextInt(size-2)+1;
			int q2 = RANDOM.nextInt(size - 1 - q1) + q1;
			//retain elems from parents in order starting from second point 
			for(int i=q2;i<p1.size()+q2;i++) {
				l1.add(p2.get(i%size));
				l2.add(p1.get(i%size));
			}
			//swap elems between points
			for(int i=q1;i<q2;i++) {
				c1.set(i,p1.get(i));
				l1.remove(new Integer(p1.get(i)));
				c2.set(i,p2.get(i));
				l2.remove(new Integer(p2.get(i)));
			}
			//insert the elems from each parent to another
			for(int i=q2;i<size+q1;i++) {
				c1.set(i%size, l1.get(0));
				l1.remove(0);
				c2.set(i%size, l2.get(0));
				l2.remove(0);
			}
			List<List<Integer>> res = new ArrayList<>();
			res.add(c1);
			res.add(c2);
			return res;
			}
			
		/*
		 * Partially Mapped Crossover
		 * IN: two parents
		 * OUT: two chromosomes resulted by Partially Mapped Cross
		 */
		List<List<Integer>> pmxCross(List<Integer> p1,List<Integer> p2){
			List<Integer> c1 = new ArrayList<>(Collections.nCopies(towns.size(), -1)),
                    c2 = new ArrayList<>(Collections.nCopies(towns.size(), -1)),
                    m1 = new ArrayList<>(Collections.nCopies(towns.size(), -1)),
                    m2 = new ArrayList<>(Collections.nCopies(towns.size(), -1));
			int size=p1.size();
			int q1 = RANDOM.nextInt(size-2)+1;
			int q2 = RANDOM.nextInt(size - 1 - q1) + q1;
			//set mappings
			for(int i=q1;i<q2;i++) {
				m1.set(p2.get(i), p1.get(i));
				m2.set(p1.get(i), p2.get(i));
				c1.set(i, p2.get(i));
				c2.set(i, p1.get(i));
			}
			//replace mappings
			for(int i=q2;i<size+q1;i++) {
				int j=i%size;
				//if find mapping
				if(m1.get(p1.get(j))!= -1) {
					c1.set(j, m1.get(p1.get(j)));
					//while a map are mapping another mapped element
					while(m1.get(c1.get(j))!= -1) 
						c1.set(j, m1.get(c1.get(j)));
				}
				else
					c1.set(j, p1.get(j));
				if(m2.get(p2.get(j))!= -1) {
					c2.set(j, m2.get(p2.get(j)));
					while(m2.get(c2.get(j))!= -1) 
						c2.set(j, m2.get(c2.get(j)));
				}
				else
					c2.set(j, p2.get(j));
			}
			List<List<Integer>> res = new ArrayList<>();
			res.add(c1);
			res.add(c2);
			return res;
		}
		
		private int turnir(List<List<Integer>>p) {
			int bestIndex=0;
			int nr=10;
			//if don't have enough parents select from how many remained
			if(p.size()<10)
				nr=p.size();
			//selecting best parent from 10 or less individuals
			for(int i=0;i<nr;i++) {
				int x = RANDOM.nextInt(nr);
				if(getTheFitnessOfASolution(p.get(x))<getTheFitnessOfASolution(p.get(bestIndex)))
					bestIndex=x;
			}
			return bestIndex;
		}
		
		private List<List<Integer>> selectParents(List<List<Integer>> p){
			//will be n/2 pair of parents
			int n = (int)(p.size()/2);
			List<List<Integer>> parents = new ArrayList<>();
			for(int i=0;i<n;i++) {
				//select first parent by turnir method
				int bestIndex=turnir(p);
				parents.add(p.get(bestIndex));
				p.remove(bestIndex);
				//select second parent random
				int x = RANDOM.nextInt(p.size());
				parents.add(p.get(x));
				p.remove(x);
				}
			return parents;
		}
		
		
		private List<List<Integer>> survivalSelection(List<List<Integer>> p,List<List<Integer>> px,List<List<Integer>> pm,int N){
			List<List<Integer>> l = new ArrayList<>();
			List<List<Integer>> population = new ArrayList<>();
			//add all individuals in selection list
			for( List<Integer> sublist : p) {
			    l.add(new ArrayList<>(sublist));
			}
			for( List<Integer> sublist : px) {
			    l.add(new ArrayList<>(sublist));
			}
			for( List<Integer> sublist : pm) {
			    l.add(new ArrayList<>(sublist));
			}
			
			//select best solution 
			List<Integer> best = l.get(0);
			for(List<Integer> sublist : l) 
				if(getTheFitnessOfASolution(sublist)<getTheFitnessOfASolution(best))
					best=sublist;
			population.add(best);
			//select the rest by turnir method
			for(int i=0;i<N;i++) {
				int bestIndex=turnir(l);
				population.add(l.get(bestIndex));
				l.remove(bestIndex);
			}
			return population;	
		}
		
		//get best fitness from a population
		private double getBestIndividual(List<List<Integer>> population) {
	        return population
	                .stream()
	                .map(this::getTheFitnessOfASolution)
	                .mapToDouble(fitness -> fitness)
	                .min()
	                .getAsDouble();
	    }
		
		
		@Override
	    public Double call() {
	        System.out.println("Started Thread: " + Thread.currentThread().getName());
	        Double result = solveTSP();
	        System.out.println(Thread.currentThread().getName() + " finished working... RESULT: " + result);
	        return result;
	    }
		
	 }
    

