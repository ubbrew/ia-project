package solvers;

import java.util.concurrent.ThreadFactory;

public class TSPSolverThreadFactory implements ThreadFactory {
    private static int count = 0;
    private static String NAME = "TSP SOLVER THREAD NUMBER - ";

    @Override
    public Thread newThread(Runnable runnable) {
        return new Thread(runnable, NAME + ++count);
    }
}
