package model;

public class Town {
    private int id;
    private double x;
    private double y;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Town() {
    }

    @Override
    public String toString() {
        return "model.Town{" +
                "id=" + id +
                ", x=" + x +
                ", y=" + y +
                '}';
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public Town(int id, double x, double y) {
        this.id = id;
        this.x = x;
        this.y = y;
    }

}
